import { createStackNavigator, createSwitchNavigator } from 'react-navigation';
import SignUp from './src/pages/SignUp/signup';
import LogIn from './src/pages/LogIn/login';
import Chat from './src/pages/chat/chat';
import LoginState from './src/pages/LoginState/login-state';

var signupStack = createStackNavigator({
  LoginStateScreen: LoginState,
  LoginScreen: LogIn,
  SignUpScreen: SignUp
});

var chatStack = createStackNavigator({
  ChatScreen: Chat
});

var rootNavigation = createSwitchNavigator({
  SignupStack: signupStack,
  ChatStack: chatStack
})

export default rootNavigation;