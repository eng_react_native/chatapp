import * as firebase from 'firebase';
import { errorMessageService } from "./error-message";
import { AsyncStorage } from "react-native"

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyClHvGcCQceopDHyVsDnIIm56FZO15PuY8",
    authDomain: "mychat-df3f0.firebaseapp.com",
    databaseURL: "https://mychat-df3f0.firebaseio.com",
    projectId: "mychat-df3f0",
    storageBucket: "mychat-df3f0.appspot.com",
    messagingSenderId: "785963474083"
};

firebase.initializeApp(firebaseConfig);
const USER_STORAGE_NAME = "USER_DATA";

export const userService = {

    signUp: (userModel) => {
        return new Promise((resolve, reject) => {
            firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(
                userModel.email,
                userModel.password
            ).then(credential => { // Promisse executou com sucesso
                resolve(credential);
            }).catch(error => { // Ocorreu erro no processamento
                var messageToShow = "";
                console.log(error.message);
                switch (error.message) {
                    case "The email address is badly formatted.":
                        messageToShow = errorMessageService
                                            .getErrorByName("invalid_email");
                        break;
                    case "Password should be at least 6 characters":
                        messageToShow = errorMessageService
                                        .getErrorByName("password_min_6_digits");
                        break;
                    default: 
                        messageToShow = "Ocorreu um erro inesperado.";
                        break;
                }
                reject(messageToShow);
            });
        });
    },

    login: (email, password) => {
        return new Promise((resolve, reject) => {
            firebase.auth()
                .signInWithEmailAndPassword(email, password)
                    .then(credentials => {
                        var userObject = {
                            email: email,
                            uid: credentials.user.uid
                        }
                        userService
                            .setUser(userObject)
                            .then(() => {
                                resolve(credentials)
                            });
                    }).catch(error => {
                        reject(error);
                    });
        })
    },

    sendMessage: (message) => {
        var newMessage = {
            message: message,
            date: new Date().toISOString()
        }

        return new Promise((resolve, reject) => {
            userService.getUser().then(user => {
                newMessage = {
                    ...newMessage,
                    uid: user ? user.uid : ""
                }

                firebase
                    .database()
                    .ref("messages") /* Referência a tabela ou cria uma nova caso ela não exista */ 
                    .push(newMessage /*Nova mensagem*/, (error) => {
                        if(error){
                            reject(error);
                        }
                        resolve(true);
                    })
            })
        })
    },

    getMessages: () => {
        return firebase
                .database()
                .ref("messages")
    },

    setUser: (usermodel) => {
        return new Promise((resolve, reject) => {
            AsyncStorage
                .setItem(USER_STORAGE_NAME, JSON.stringify(usermodel))
                .then(() => {
                    resolve(true);
                }).catch(error => {
                    reject(error);
                })
        })
    }, 
    
    getUser: () => {
        return new Promise((resolve, reject) => {
            AsyncStorage
                .getItem(USER_STORAGE_NAME)
                .then(user => {
                    resolve(JSON.parse(user));
                }).catch(error => {
                    reject(error);
                })
        })
    },

    logout: () => {
        return new Promise((resolve, reject) => {
            // AsyncStorage.removeItem(USER_STORAGE_NAME)
            AsyncStorage
                .clear()
                .then(() => {
                    resolve(true);
                });
        })
    }

}