import erroMessages from '../shared/common/error-messages.json'

export const errorMessageService = {
    getErrorByName: (messageName ,language = "pt-br" /*Parâmetro opicional*/) => {
        return erroMessages[language][messageName];
    }
}