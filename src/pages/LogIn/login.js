import React from 'react';
import { StyleSheet, Text, View, TextInput, Button, TouchableHighlight } from 'react-native';
import { userService } from "../../services/user-service";
import Loading from '../../shared/components/loading';

export default class LogIn extends React.Component {
    static navigationOptions = (navigation) => {
        return {
            header: null
        }
    }
    state = {
        username: "",
        password: "",
        error: null,
        loading: false
    }

    login() {
        userService
            .login(this.state.username, this.state.password)
                .then(credentials => {
                    console.log(credentials);
                    this.showHideLoading(false);
                    this.props.navigation.navigate("ChatStack");
                }).catch(error => {
                    this.showHideLoading(false);
                    alert(error.message);
                    console.log(error.message);
                });
    }

    showHideLoading(visible = true) {
        this.setState({
            loading: visible
        });
    }

    render() {
        return (
            <Loading loading={this.state.loading}>
                <View style={styles.logoContainer}>
                    <TouchableHighlight 
                        style={{
                            borderRadius: 500
                        }}
                        onPress={() => {
                            alert("icon clicked");
                        }}>
                        <View style={styles.logo}></View>
                    </TouchableHighlight>
                </View>
                <View>
                    <TextInput
                        style={styles.textInput}
                        placeholder={"Username"}
                        autoCapitalize={'none'}
                        onChangeText={(username) => {
                            this.setState({ username });
                        }}
                    />

                    <TextInput
                        style={styles.textInput}
                        placeholder={"Password"}
                        secureTextEntry={true}
                        onChangeText={(password) => {
                            this.setState({ password });
                        }}
                    />
                    {
                        this.state.error ? <View>
                            <Text style={{
                                color: "red",
                                textAlign: "center"
                            }}>{this.state.error}</Text>
                        </View> : null
                    }
                    <Button
                        title={"Login"}
                        onPress={() => {
                            this.setState({
                                error: null,
                                loading: true
                            }, () => this.login());
                        }}
                    />

                    <Button
                        title={"Cadastre-se"}
                        onPress={() => {
                            this.props.navigation.navigate("SignUpScreen");
                        }}
                    />
                </View>
                
            </Loading>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logoContainer: {

    }, logo: {
        width: 100,
        height: 100,
        backgroundColor: "red",
        borderWidth: 1,
        borderRadius: 500,
        marginBottom: 30
    },
    textInput: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        width: 250,
        marginBottom: 10,
        paddingLeft: 10
    }
});
